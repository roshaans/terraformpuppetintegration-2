provider "aws" {
  region = "us-east-1"
}

resource "aws_instance" "postgresql_terraform_puppet" {
    ami = "ami-0440d3b780d96b29d"
    instance_type = "t2.micro"
    key_name = "myec2key"

    provisioner "local-exec" {
        command = "puppet apply ../puppet/example.pp"
    }

}