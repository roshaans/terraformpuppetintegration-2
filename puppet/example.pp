# puppet/example.pp

# Install PostgreSQL Server package
package { 'postgresql':
  ensure => installed,
  source => "C:\Users\jayav\Downloads\postgresql-16.2-1-windows-x64.exe", # Example source URL
}

# Ensure PostgreSQL service is running and enabled
service { 'postgresql':
  ensure => running,
  enable => true,
}